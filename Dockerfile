FROM python:3.6
RUN pip install flask
COPY . .
ENTRYPOINT FLASK_APP=app.py flask run --host=0.0.0.0 --port=8080

